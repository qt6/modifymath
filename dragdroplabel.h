#ifndef DRAGDROPLABEL_H
#define DRAGDROPLABEL_H

#include <QLabel>

class DragLabel : public QLabel
{
    Q_OBJECT
public:
    explicit DragLabel(const QString &text = QString(), QWidget *parent = nullptr);

protected:

    virtual void mousePressEvent(QMouseEvent *e) override;
};

class DropLabel : public QLabel
{
    Q_OBJECT
public:
    explicit DropLabel(const QString &text = QString(), QWidget *parent = nullptr);

protected:

    virtual void dragEnterEvent(QDragEnterEvent *e) override;
    virtual void dropEvent(QDropEvent *e) override;
};

#endif // DRAGDROPLABEL_H
