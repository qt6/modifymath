#include "centralwidget.h"

#include <QDebug>

#include <QTimer>
#include <QApplication>
#include <QShowEvent>

#include "options.h"
#include "mainwidget.h"
#include "settingsdialog.h"
#include "resultwidget.h"

CentralWidget::CentralWidget(QWidget *parent)
    : QStackedWidget(parent)
{

    m_mainWidget = new MainWidget(this);
    m_resultWidget = new ResultWidget(this);

    addWidget(m_mainWidget);
    addWidget(m_resultWidget);

    connect(m_mainWidget, &MainWidget::finished,[this]{

        m_resultWidget->updateResults();

        setCurrentWidget(m_resultWidget);
    });

    QTimer::singleShot(100,[this]{
        SettingsDialog d;

        if(d.exec())
            m_mainWidget->start();
        else
            close();
    });
}
