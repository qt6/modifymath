#ifndef OPTIONS_H
#define OPTIONS_H

class QJsonObject;
class QJsonArray;

#include <QList>
#include <QByteArray>
#include <QtCore>
#include <QTime>

#include "info.h"

struct Paths
{
    static void init();
    static QString get(const QString &key);
    static QString options;
};


class OptionSaver
{
public:
    OptionSaver();
    ~OptionSaver();

    static bool load(QJsonObject &json, const QString &fileName);

    static bool save(const QJsonObject &json, const QString &fileName);
};

struct PersonalConfig
{

    PersonalConfig(const QString &name = QString())
    {
        this->name = name;
    }

    QJsonObject save() const;
    void load(const QJsonObject &json);

    bool operator==(const PersonalConfig &other) const
    {
        return !name.compare(other.name);
    }

    QString name;

    int countOperands {0};
    int minVal {0};
    int maxVal {0};
    int count {0};
    int lost {0};
    bool repeat {false};
    bool mix {false};
    bool samples {false};
    bool inequlaity {false};
    bool isPlus {false};
    bool isMinus {false};
    bool isMult {false};
    bool isDev {false};
    bool isColumn {false};
    bool isLimited {false};
    QTime bestTime {QTime(0,0,0)};
    QTime limitTime {QTime(0,0,0)};

    QByteArray signs;

    QList<SampleExtInfo> resultsSamples;
    QList<InequalityInfo> resultsInequality;

};
Q_DECLARE_METATYPE(PersonalConfig)

class PersonalOptions
{
public:

    static inline PersonalOptions &global(){
        static PersonalOptions opt;
        return opt;
    }

    void reset();

    QJsonArray save() const;
    void load(const QJsonArray &json);


    QList<PersonalConfig> configs;



private:
    PersonalOptions();
};

class GeneralOptions
{
public:

    static inline GeneralOptions &global(){
        static GeneralOptions opt;
        return opt;
    }

    void reset();

    QJsonObject save() const;
    void load(const QJsonObject &json);

    PersonalConfig lastConfig;


    static int randomVal(int minVal, int maxVal, bool includeMax = true);
    static int randomVal(int maxVal, bool includeMax = true);


private:
    GeneralOptions();

};

#endif // OPTIONS_H
