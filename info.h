#ifndef INFO_H
#define INFO_H


#include <QVector>

struct CommonInfo
{
    bool isRight {false};
};


struct SampleInfo
{
    QVector<int> vals;
    QByteArray signs;

    int resVal {0};
};


struct SampleExtInfo : public SampleInfo,  public CommonInfo
{

    static SampleExtInfo &global()
    {
        static SampleExtInfo sampleExt;
        return sampleExt;
    };

    int ansVal {0};
};

struct InequalityInfo : public CommonInfo
{

    static InequalityInfo &global()
    {
        static InequalityInfo inEq;
        return inEq;
    };


    SampleInfo leftExample;
    SampleInfo rightExample;

    char resSign {0};
    char ansSign {0};
};



#endif // INFO_H
